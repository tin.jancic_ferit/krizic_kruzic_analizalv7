﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        bool turn = true;//kada je true onda je X na redu, false O
        int turnCount = 0;
        static String player1, player2; //spremiti ce imena igraca
        public Form1()
        {
            InitializeComponent();
        }

        public static void PlayerNames(String n1, String n2)
        {
            player1 = n1;
            player2 = n2;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;//vrijednost sender-a stavljamo u varijablu button
            turnCount++;//broji poteze, potrebno za nerijesen rezultat, ako je turnCount = 9
            if (turn == true)
            {
                button.Text = "X";
            }
            else button.Text = "O";
            turn = !turn;//kada jedan igrac odigra, drugi igrac ima pravo na potez
            button.Enabled = false;//osigurava da se isti button ne moze vise puta pritisnuti
            checkTheResult();
        }

        private void checkTheResult()
        {
            bool winner = false;
            //provjera pobjednika po redovima(A = prvi red, B = drugi, C = treci)
            if((A1.Text == A2.Text) && (A2.Text == A3.Text)
                && !(A1.Enabled && A2.Enabled && A3.Enabled))
            {
                winner = true;
            }
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text)
                && !(B1.Enabled && B2.Enabled && B3.Enabled))
            {
                winner = true;
            }
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text)
                && !(C1.Enabled && C2.Enabled && C3.Enabled))
            {
                winner = true;
            }
            //provjera pobjednika po stupcima(1 = prvi stupac, 2 = drugi, 3 = treci)
            if ((A1.Text == B1.Text) && (B1.Text == C1.Text)
                && !(A1.Enabled && B1.Enabled && C1.Enabled))
            {
                winner = true;
            }
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text)
                && !(A2.Enabled && B2.Enabled && C2.Enabled))
            {
                winner = true;
            }
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text)
                && !(A3.Enabled && B3.Enabled && C3.Enabled))
            {
                winner = true;
            }
            //provjera pobjednika po dijagonalama
            if ((A1.Text == B2.Text) && (B2.Text == C3.Text)
                && !(A1.Enabled && B2.Enabled && C3.Enabled))
            {
                winner = true;
            }
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text)
                && !(A3.Enabled && B2.Enabled && C1.Enabled))
            {
                winner = true;
            }
            //ispis
            if (winner == true)
            {
                
                string winner_string = "";
                if (turn == true)
                {
                    winner_string = player2;//jer je player2 uvijek O, player1 X
                    //azuriranje labele na kojoj se broje pobjede drugog igraca
                    //parsiramo tako da string pretvorimo u broj i onda ga zbrajamo s 1
                    //na kraju pretvaramo nazad u string tako da se moze zapisati u labelu
                    p2_win_count.Text = (Int32.Parse(p2_win_count.Text) + 1).ToString();
                }
                else
                {
                    winner_string = player1;
                    p1_win_count.Text = (Int32.Parse(p1_win_count.Text) + 1).ToString();
                }
                    //ispis u slucaju pobjede
                MessageBox.Show(winner_string + " Wins!", "Result");
                disableButtons();
            }
            else if(turnCount == 9)
            {
                MessageBox.Show("Draw!", "Result");
                draw_count.Text = (Int32.Parse(draw_count.Text) + 1).ToString();
            }
        }
        //metoda kojom se nakon necije pobjede onemogucava nastavak igranja
        private void disableButtons()
        {
            foreach (Control c in Controls)
            {
                    try//try blok jer baca iznimku jer smo pokusali kod disable-anja
                       //castati nesto sto nije gumb(File izbornik) u gumb
                    {
                        Button b = (Button)c;
                    b.Enabled = false;//disable-anje svakog gumba
                    }
                    catch { }
                    //hvatamo bilo koju iznimku, ali program nastavlja s izvrsavanjem ostalog
            }
        }
        //postavke za novu igru(reset svih gumbova, broja pokusaja...)
        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;//x uvijek ide prvi
            turnCount = 0;
            
                foreach (Control c in Controls)
                {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;//moguce ponovno pritiskanje
                    b.Text = "";//da na gumbima ne pise nista
                }
                catch { }
            }
            
        }
        //metoda koja se poziva kada mis prelazi preko gumba,
        //ime osobe koja je na redu treba pisati tamo
        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn == true)
                {
                    b.Text = "X";
                }
                else b.Text = "O";
            }
        }
        //metoda koja se poziva kada mis vise nije na gumbu
        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = ""; //kada se mis makne gumb ostaje prazan
            }
        }
        //metoda kojom u izborniku mozemo izabrati reset
        //ona vraca sve brojace pobjeda i nerijesenog rezultata na nulu
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            p1_win_count.Text = "0";
            draw_count.Text = "0";
            p2_win_count.Text = "0";
        }

        //metoda koja objasnjava sto se dogada kada ucitamo aplikaciju
        private void Form1_Load(object sender, EventArgs e)
        {
            PlayersName pN = new PlayersName();//objekt PlayersName forme koja 
                                               //sluzi za upis imena igraca
            pN.ShowDialog();//osigurava neigranje igre dok ne upisemo imena

            label1.Text = player1;
            label2.Text = player2;
        }
    }
}
